/*
'------------------------------------------------------------------------------------------------------------------------
'Script Name:      pnid2017_check_cruise_score.js
'Date Generated:   Created  5/9/2017
'Purposes:         Score 2017 check cruise data in InventoryManager
'
'Revision History:
'2. Removed isScorable. Put in return of 0 for dbhScore and htScore if any DBH or THT is undefined. Put in logic to return scores of 0 if DBH or THT are 0.
'3. Change undefined to null
'4. Added species score
'5. Added site, crown and tree age
'6. Add group, defect, and added tree scores
'------------------------------------------------------------------------------------------------------------------------
*/

// main function for calculating cruise score.  Loops through all stands in an input dataset
// then all plots for each stand, then all treepairs for each plot.  Currently only calls the
// tree level score calculation (treeScore) but will be expanded to include scoring at the
// plot and stand level
function calcScore(data){
  var out = new CruiseResults();
  // loop through stands
  var stands = data.stands;
  for (var stand in stands) {
    if (stands.hasOwnProperty(stand)) {
      var thisStand = stands[stand];
      out.stands[stand] = {plots:{}};
      var plotCount = {};
	  var sumPlotScores = {};
      for (var plot in thisStand.plots) {
        if (thisStand.plots.hasOwnProperty(plot)) {
          var thisPlot = thisStand.plots[plot];
     	  var cruiserId = thisPlot.CRSRID;
		  var thisPlotNumTrees = Object.keys(thisPlot.trees).length;
          out.stands[stand].plots[plot] = {trees:{}};
          //out.stands[stand].plots[plot]['treeSum'] = 0;
          //out.stands[stand].plots[plot]['plotSum'] = 0;
		  // get plot level scores
		  var plotFlaggingScore = 0;
		  var plotStockingScore = 0;
		  var plotHabitatScore = 0;
		  if (typeof thisPlot.FlaggedOK != 'undefined' && thisPlot.FlaggedOK !== null) {
			 if (thisPlot.FlaggedOK.trim() === "N" || thisPlot.FlaggedOK.trim() === "No") {
		        plotFlaggingScore = 5;
			 }
	      }
		  if (typeof thisPlot.ChkPStkID != 'undefined' && thisPlot.ChkPStkID !== null) {
			 if (thisPlot.PSTKID === 1 && thisPlot.ChkPStkID.trim() !== "Stocked/Forested") {
		        plotStockingScore = 3;
			 }	else if (thisPlot.PSTKID == 2 && thisPlot.ChkPStkID.trim() !== "Non-stocked/Forested") {
		        plotStockingScore = 3;
			 }	else if (thisPlot.PSTKID == 3 && thisPlot.ChkPStkID.trim() !== "Non-forested") {
		        plotStockingScore = 3;
			 }

	      }
		  if (typeof thisPlot.ChkPHabTyID != 'undefined' && thisPlot.ChkPHabTyID !== null) {
			 if (thisPlot.PHABTYID != thisPlot.ChkPHabTyID) {
		        plotHabitatScore = 5;
			 }
	      }
          // loop through all tree pairs
		  var treeSumScores = 0;
          for (var treePair in thisPlot.trees) {
            if (thisPlot.trees.hasOwnProperty(treePair)) {
              var thisTreePair = thisPlot.trees[treePair];
			  var tScore =  treeScore(thisTreePair.A,thisTreePair.B,thisStand.BAF,thisPlotNumTrees);
		      out.stands[stand].plots[plot].trees[treePair] = tScore;
              treeSumScores += tScore.score;
              //alert(JSON.stringify(thisTreePair.A));
            }
          }
		  var thisPlotScore = plotFlaggingScore + plotStockingScore + plotHabitatScore + treeSumScores;
		  if(typeof sumPlotScores[cruiserId] == 'undefined'){
			  sumPlotScores[cruiserId] = thisPlotScore;
			  plotCount[cruiserId] = 1;
		  } else {
			  sumPlotScores[cruiserId] += thisPlotScore;
			  plotCount[cruiserId] += 1;
		  }
          out.stands[stand].plots[plot]['score'] = thisPlotScore;
          out.stands[stand].plots[plot]['FlaggedOKscore'] = plotFlaggingScore;
          out.stands[stand].plots[plot]['PlotStockingScore'] = plotStockingScore;
          out.stands[stand].plots[plot]['PlotHabitatScore'] = plotHabitatScore;
          out.stands[stand].plots[plot]['PlotTreeScore'] = treeSumScores;
          out.stands[stand].plots[plot]['CRSRID'] = thisPlot['CRSRID'];
        }
      }
	  for (var cruiser in sumPlotScores){
		if(sumPlotScores.hasOwnProperty(cruiser)){
         //out.stands[stand][cruiser] = sumPlotScores.cruiser / plotCount.cruiser;
         out.stands[stand][cruiser] = sumPlotScores[cruiser] / plotCount[cruiser];
		}
	  }
      //out.stands[stand]['score'] = sumPlotScores / plotCount;
	}
  }
  //alert(JSON.stringify(out, null, 2));
  return out;
}

function CruiseResults() {
    this.stands = {};
}

function treeScore(tree1, tree2, BAF, numTrees){
  var out = {};

  var dbhScore = 0;
  var htScore = 0;
  var spScore = 0;
  var crnScore = 0;
  var siteHtScore = 0;
  var treeAgeScore = 0;
  var gpScore = 0;
  var defScore = 0;
  var addedTreeScore = 0;
  var missingTreeScore = 0;
  var score = 0;

  if (tree1 === null) {
	missingTreeScore = calcMissingTreeScore(tree2.DBH,BAF,numTrees,tree2.CNT);
  } else {
    addedTreeScore = calcAddedTreeScore(tree1.CNT,tree2.CNT,tree1.DBH,BAF,numTrees);
	if (addedTreeScore > 0) {
      dbhScore = 0;
      htScore = 0;
      spScore = 0;
      crnScore = 0;
      siteHtScore = 0;
      treeAgeScore = 0;
      gpScore = 0;
      defScore = 0;
	} else {	
      dbhScore = calcDbhScore(tree1.DBH,tree2.DBH);
      htScore = calcHtScore(tree1.THT,tree2.THT,tree1.GP);
      spScore = calcSpScore(tree1.SP,tree2.SP);
      crnScore = calcCrownScore(tree1.CRN,tree2.CRN);
      siteHtScore = calcSiteHtScore(tree1.THT,tree2.THT,tree1.GP);
      treeAgeScore = calcTreeAgeScore(tree1.AGE,tree2.AGE);
      gpScore = calcGpScore(tree1.GP,tree2.GP);
      defScore = calcDefectScore(tree1.DEF1,tree2.DEF1,3) + calcDefectScore(tree1.DEF2,tree2.DEF2,2) + calcDefectScore(tree1.DEF3,tree2.DEF3,1);
	}
  }
  score = htScore + dbhScore + spScore + crnScore + siteHtScore + treeAgeScore + gpScore + defScore + addedTreeScore + missingTreeScore;
  out.score = score;
  out.dbhScore = dbhScore;
  out.htScore = htScore;
  out.spScore = spScore;
  out.crnScore = crnScore;
  out.siteHtScore = siteHtScore;
  out.treeAgeScore = treeAgeScore;
  out.gpScore = gpScore;
  out.defScore = defScore;
  out.addedTreeScore = addedTreeScore;
  out.missingTreeScore = missingTreeScore;
  //	alert("tree: " + score  + ", Dbh: " + dbhScore + ", Height: "  + htScore + ", Species: " + spScore );
  //	alert("crown: " + crnScore  + ", Site Ht: " + siteHtScore + ", Tree Age: "  + treeAgeScore + ", Group: " + gpScore + ", Defect: " + defScore + ", addedTree: " + addedTreeScore);
  return(out);
}

function calcDbhScore(A,B){
  var X = 0;
  var PEN = 2;
  //Put in round to handle floating point
  var Diff = Math.round(Math.abs(A - B) * 10);

  if (B === null || A === null) {
	X = 0;
  } else if(Diff > 4) {
	X = PEN;
  } else {
	X = 0;
  }
  return X;
}

function calcHtScore(A,B,Z1){
  var X = 0;
  var PEN = 2;
  var Diff = Math.abs(A - B);
  var P = 0;

  //Get percentage
  if(B > 0) {
	P = (Diff / B) * 100;
  } else {
	P = 0;
  }

  //Get score
  if (B === null || A === null || Z1 === "OS" || Z1 === "OF" || B == 0 || A == 0) {
    X = 0;
  } else if (B <= 50) {
    if (P > 5) {
	  X = PEN;
	}
  } else if (B <= 150) {
    if (P > 7) {
	  X = PEN;
	}
  } else if (B > 150) {
    if (P > 9) {
	  X = PEN;
	}
  }
  return X;
}

function calcSpScore(A,B) {
  var X = 0;
  var PEN = 3;

  if (B === null || A === null) {
    X = 0;
  } else if (A != B) {
    X = PEN;
  }
  return X;
}

function calcSiteHtScore(A,B,Z1){
  var X = 0;
  var PEN = 3;
  var Diff = Math.abs(A - B);
  var P = 0;

  //Get percentage
  if(B > 0) {
	P = (Diff / B) * 100;
  } else {
	P = 0;
  }

  //Get score
  if (B === null || A === null || (Z1 != "OS" && Z1 != "OF")) {
    X = 0;
  } else if (P > 5) {
    X = PEN;
  }
  return X;
}

function calcCrownScore(A,B){
  var X = 0;
  var PEN = 2;
  var Diff = Math.round(Math.abs(A - B) * 10);

  //Get score
  if (B === null || A === null) {
    X = 0;
  } else if (Diff > 200) {
    X = PEN;
  }
  return X;
}

function calcTreeAgeScore(A,B){
  var X = 0;
  var PEN = 1;
  var Diff = Math.round(Math.abs(A - B) * 10);

  //Get score
  if (B === null || A === null || B == 0) {
    X = 0;
  } else if (B < 20) {
    if (Diff > 30) {
	  X = PEN;
	}
  } else if (B <= 50) {
    if (Diff > 50) {
	  X = PEN;
	}
  } else if (B > 50) {
    if (Diff > 100) {
	  X = PEN;
	}
  }
  return X;
}

function calcGpScore(A,B){
  var X = 0;
  var PEN = 1;

  //Get score
  if (B === null || A === null) {
    X = 0;
  } else if (A != B) {
    X = PEN;
  }
  return X;
}

function calcDefectScore(A,B,P){
  var X = 0;
  var Diff = Math.round(Math.abs(A - B) * 10);

  if (B === null || A === null) {
	X = 0;
  } else if(Diff > 100) {
	X = P;
  } else {
	X = 0;
  }
  return X;
}

function calcAddedTreeScore(A,B,Z1,Z2,Z3){
  var X = 0;
  var PEN = 0;

  if (B === null || A === null) {
    X = 0;
  else if (A >= 1 && B === 0) {
    if (Z1 < 4.6 && Z2 !== 999) {
	  PEN = 4;
    } else if (Z2 === 999 && Z3 >= 11) {
      PEN = 6;
    } else {
      PEN = 12;
    }
  }
  X = PEN;
  return X;
}

function calcMissingTreeScore(Z1,Z2,Z3,Z4){
  var X = 0;
  var PEN = 0;
  
  if (Z4 > 0) {
    if (Z1 < 4.6 && Z2 !== 999) {
	  PEN = 4;
    } else if (Z2 === 999 && Z3 >= 11) {
      PEN = 6;
    } else {
      PEN = 12;
    }
  }
  X = PEN;
  return X;
}

var testData1 = {
  "stands":{
    "15508":{
      "plots":{
        "211385":{
          "trees":{
            "1":{"A":{"TREE":1,"SP":"PP","DBH":11.4,"CNT":1,"THT":48,"CRN":50,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":1,"SP":"PP","DBH":11.4,"CNT":1,"THT":48,"CRN":50,"H":1,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "2":{"A":{"TREE":2,"SP":"PP","DBH":10.6,"CNT":1,"THT":44,"CRN":40,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":2,"SP":"PP","DBH":10.8,"CNT":1,"THT":44,"CRN":40,"H":1,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "3":{"A":{"TREE":3,"SP":"PP","DBH":9.6,"CNT":1,"THT":42,"CRN":40,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":3,"SP":"PP","DBH":10.4,"CNT":1,"THT":44,"CRN":40,"H":1,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "4":{"A":{"TREE":4,"SP":"PP","DBH":8.8,"CNT":1,"THT":null,"CRN":40,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":4,"SP":"PP","DBH":8.8,"CNT":1,"THT":0,"CRN":30,"H":0,"AGE":0,"A":0,"DEF1":10,"DEF2":10,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "5":{"A":{"TREE":5,"SP":"PP","DBH":10.1,"CNT":1,"THT":null,"CRN":30,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":5,"SP":"PP","DBH":10.3,"CNT":1,"THT":0,"CRN":30,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "6":{"A":{"TREE":6,"SP":"PP","DBH":9.5,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":6,"SP":"PP","DBH":9.5,"CNT":1,"THT":0,"CRN":40,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "7":{"A":{"TREE":7,"SP":"PP","DBH":8.5,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":7,"SP":"PP","DBH":8.5,"CNT":1,"THT":0,"CRN":30,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "8":{"A":{"TREE":8,"SP":"PP","DBH":9.6,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":8,"SP":"PP","DBH":9.7,"CNT":1,"THT":0,"CRN":40,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "9":{"A":{"TREE":9,"SP":"PP","DBH":7.1,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":9,"SP":"PP","DBH":7.1,"CNT":1,"THT":0,"CRN":30,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "10":{"A":{"TREE":10,"SP":"PP","DBH":8.2,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":10,"SP":"PP","DBH":8.3,"CNT":1,"THT":0,"CRN":30,"H":0,"AGE":0,"A":0,"DEF1":10,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "11":{"A":{"TREE":11,"SP":"PP","DBH":8.2,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":11,"SP":"PP","DBH":8.3,"CNT":1,"THT":0,"CRN":40,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}},
            "12":{"A":{"TREE":12,"SP":"PP","DBH":8.2,"CNT":1,"THT":null,"CRN":null,"H":null,"AGE":0,"A":null,"DEF1":null,"DEF2":null,"DEF3":null,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"C"},"B":{"TREE":12,"SP":"PP","DBH":8.1,"CNT":1,"THT":0,"CRN":30,"H":0,"AGE":0,"A":0,"DEF1":0,"DEF2":0,"DEF3":0,"undefined":"{FE715F41-B885-4C8A-9DBC-28E9813A187F}","Cruise_Type":"S"}
            }
          }
        }
      }
    }
  }
};
